<?php
namespace Yodasoft\Logger;

use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;

/**
 * a simple logging class.
 *
 * Usage:
 * $log = new Yodasoft\Logger\Logger('/var/log/', Psr\Log\LogLevel::INFO);
 * $log->info('test info');
 *
 * @author  Yoda <yodarichard@outlook.com>
 * @since   May 26, 2023
 * @link    https://gitee.com/yodarichard/logger
 * @version 1.0.1
 */

/**
 * Class
 */
class Logger extends AbstractLogger
{
    /**
     * Options should be settable
     * @var array
     */
    protected $options = array (
        'dateFormat'     => 'Y-m-d H:i:s.u',
        'filename'       => false,
        'flushFrequency' => false,
        'logFormat'      => false,
        'appendContext'  => true,
    );

    /**
     * Current minimum logging threshold.
     * @var integer
     */
    protected $logLevelThreshold = LogLevel::DEBUG;

    /**
     * Log Levels.
     * @var array
     */
    protected $logLevels = array(
        LogLevel::EMERGENCY => 0,
        LogLevel::ALERT     => 1,
        LogLevel::CRITICAL  => 2,
        LogLevel::ERROR     => 3,
        LogLevel::WARNING   => 4,
        LogLevel::NOTICE    => 5,
        LogLevel::INFO      => 6,
        LogLevel::DEBUG     => 7
    );

    /**
     * The number of lines logged in this instance.
     * @var int
     */
    private $logLineCount = 0;

    /**
     * Holds the file handle for this instance.
     * @var resource
     */
    private $fileHandle;

    /**
     * Path to the log file.
     * @var string
     */
    private $logFilePath;

    /**
     * constructor
     *
     * @param string $logDirectory      File path of log directory
     * @param string $logLevelThreshold Threshold of log level 
     * @param array  $options
     */
    public function __construct($logDirectory, $logLevelThreshold = LogLevel::DEBUG, array $options = array())
    {
        $this->logLevelThreshold = $logLevelThreshold;
        $this->options = array_merge($this->options, $options);

        $logDirectory = rtrim($logDirectory, DIRECTORY_SEPARATOR);
        if(!file_exists($logDirectory)) {
            if(!mkdir($logDirectory, 0777, true)) {
                $this->exception('The directory could not be creacted.');
            }
        }

        if(0 === strpos($logDirectory, 'php://')) {
            $this->setLogToStdOut($logDirectory);
            $this->setFileHandle('w+');
        } else {
            $this->setLogFilePath($logDirectory);
            if(file_exists($this->logFilePath) && !is_writable($this->logFilePath)) {
                $this->exception('The file could not be written to.');
            }
            $this->setFileHandle('a');
        }

        if(!$this->fileHandle) {
            $this->exception('The file could not be opened.');
        }
    }

    /**
     * destructor
     */
    public function __destruct()
    {
        if($this->fileHandle) {
            fclose($this->fileHandle);
        }
    }

    /**
     * Log context by threshold level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log($level, $message, array $context = array())
    {
        if($this->logLevels[$this->logLevelThreshold] < $this->logLevels[$level]) {
            return;
        }
        $message = $this->formatMessage($level, $message, $context);
        $this->write($message);
    }

    /**
     * Get the file path that the log is currently writing to. Used for unit tests.
     *
     * @return string
     */
    public function getLogFilePath()
    {
        return $this->logFilePath;
    }

    /**
     * @param string $stdOutPath
     */
    protected function setLogToStdOut($stdOutPath) {
        $this->logFilePath = $stdOutPath;
    }

    /**
     * @param string $logDirectory
     */
    protected function setLogFilePath($logDirectory) {
        $file = $this->options['filename'] ? $this->options['filename'] : date('Ymd').'.log';
        $this->logFilePath = $logDirectory.DIRECTORY_SEPARATOR.$file;
    }

    /**
     * @param $writeMode
     */
    protected function setFileHandle($writeMode) {
        $this->fileHandle = fopen($this->logFilePath, $writeMode);
    }

    /**
     * Writes a line to the log.
     *
     * @param string $message
     * @return void
     */
    protected function write($message)
    {
        if(!$this->fileHandle) {
            return ;
        }
        if(false === fwrite($this->fileHandle, $message)) {
            $this->exception('The file could not be written to.');
            return ;
        }

        $this->logLineCount++;
        if($this->options['flushFrequency'] && 0 === $this->logLineCount % $this->options['flushFrequency']) {
            fflush($this->fileHandle);
        }
    }

    /**
     * Format the message.
     *
     * @param  string $level
     * @param  string $message
     * @param  array  $context
     * @return string
     */
    protected function formatMessage($level, $message, $context)
    {
        if($this->options['logFormat']) {
            $parts = array(
                'date'          => $this->getTimestamp(),
                'level'         => strtoupper($level),
                'level-padding' => str_repeat(' ', 9 - strlen($level)),
                'priority'      => $this->logLevels[$level],
                'message'       => $message,
            );
            if($this->options['appendContext']) {
                // $parts['context'] = json_encode($context);
                $parts['context'] = $this->indent($this->contextToString($context));
            }
            $message = $this->options['logFormat'];
            foreach($parts as $part => $value) {
                $message = str_replace('{'.$part.'}', $value, $message);
            }
        } else {
            $message = "[{$this->getTimestamp()}] [{$level}] {$message}";
            if($this->options['appendContext'] && !empty($context)) {
                $message .= PHP_EOL.$this->indent($this->contextToString($context));
            }
        }
        return $message.PHP_EOL;
    }

    /**
     * Exception, just output error message.
     *
     * @return void
     */
    private function exception($message) {
        echo $message.PHP_EOL;
    }

    /**
     * Formatted Date/Time.
     *
     * @return string
     */
    private function getTimestamp()
    {
        list($usec, $sec) = explode(" ", microtime());
        $rep = array('u'=>sprintf("%06d", $usec*100000));
        return date(strtr($this->options['dateFormat'], $rep), $sec);
    }

    /**
     * Covert extra information to string.
     *
     * @param  array $context
     * @return string
     */
    protected function contextToString($context)
    {
        $export = '';
        foreach ($context as $key => $value) {
            $export .= "{$key}: ";
            $export .= preg_replace(array(
                '/=>\s+([a-zA-Z])/im',
                '/array\(\s+\)/im',
                '/^  |\G  /m'
            ), array(
                '=> $1',
                'array()',
                '    '
            ), str_replace('array (', 'array(', var_export($value, true)));
            $export .= PHP_EOL;
        }
        return str_replace(array('\\\\', '\\\''), array('\\', '\''), rtrim($export));
    }

    /**
     * Indents string with given indent.
     *
     * @param  string $string
     * @param  string $indent
     * @return string
     */
    protected function indent($string, $indent = '    ')
    {
        return $indent.str_replace("\n", "\n".$indent, $string);
    }
    
}
