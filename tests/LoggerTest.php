<?php

use Psr\Log\LogLevel;
// use Yodasoft\Logger\Logger;
require __DIR__.'/../src/Logger.php';

class LoggerTest extends PHPUnit\Framework\TestCase
{
    private $logPath;

    private $logger;
    private $errLogger;

    public function setUp():void
    {
        $this->logPath = __DIR__.'/logs';
        $this->logger = new Logger($this->logPath, LogLevel::DEBUG, array ('flushFrequency' => 1));
        $this->errLogger = new Logger($this->logPath, LogLevel::ERROR, array (
            'filename' => 'error.log',
            'flushFrequency' => 1
        ));
    }

    public function testImplementsPsr3LoggerInterface()
    {
        $this->assertInstanceOf('Psr\Log\LoggerInterface', $this->logger);
    }

    public function testAcceptsExtension()
    {
        $this->assertStringEndsWith('.log', $this->errLogger->getLogFilePath());
    }

    public function testAcceptsPrefix()
    {
        $filename = basename($this->errLogger->getLogFilePath());
        $this->assertStringStartsWith('error', $filename);
    }

    public function testWritesBasicLogs()
    {
        $s = 'This is a test '.date("YmdHis");
        $this->logger->log(LogLevel::DEBUG, $s);
        $this->errLogger->log(LogLevel::ERROR, $s);

        $this->assertTrue(file_exists($this->errLogger->getLogFilePath()));
        $this->assertTrue(file_exists($this->logger->getLogFilePath()));

        $this->assertLastLineEquals($this->logger, $s);
        $this->assertLastLineEquals($this->errLogger, $s);
    }


    public function assertLastLineEquals(Logger $logr, $s)
    {
        $this->assertStringContainsString($s, $this->getLastLine($logr->getLogFilePath()));
    }

    public function assertLastLineNotEquals(Logger $logr, $s)
    {
        $this->assertStringNotContainsString($s, $this->getLastLine($logr->getLogFilePath()));
    }

    private function getLastLine($filename)
    {
        $last_line = exec('tail -n 1 '.$filename);
        return trim($last_line);
    }

    public function tearDown():void {
    }
    
}
